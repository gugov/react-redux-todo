let initialState = {
    todos: JSON.parse(localStorage.getItem('todos'))
}

export const todos = (state = initialState, {type, payload}) => {
    let new_arr = [];
    switch (type) {
        case 'ADD_TODO':
            state.todos.push(payload)
            new_arr = state.todos;
            localStorage.setItem('todos', JSON.stringify(new_arr))
            return {
                ...state,
                todos: new_arr
            }
        case 'REMOVE_TODO':
            localStorage.setItem('todos', JSON.stringify(state.todos.filter((item) => (item.id !== payload))))
            return {
                ...state,
                todos: state.todos.filter((item) => (item.id !== payload))
            }
        case 'CHECK_AS_DONE':
            const elementsIndex = state.todos.findIndex(element => element.id === payload);
            let newArray = [...state.todos];
            newArray[elementsIndex] = {...newArray[elementsIndex], isComplete: !newArray[elementsIndex].isComplete}
            return {
                ...state,
                todos: newArray
            }
        default:
            return state
    }
}