export const ADD_TODO = payload => ({
    type: 'ADD_TODO',
    payload: payload
})

export const REMOVE_TODO = payload => ({
    type: 'REMOVE_TODO',
    payload: payload
})

export const CHECK_AS_DONE = payload => ({
    type: 'CHECK_AS_DONE',
    payload: payload
})