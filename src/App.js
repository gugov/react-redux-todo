import React, {Component, Fragment} from 'react';
import './App.css';
import {connect} from "react-redux";
import TodoList from './components/todos/TodoList/TodoList'
import AddTodo from './components/todos/AddTodo/AddTodo'

class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Fragment>
                <TodoList todos={this.props.todos}></TodoList>
                <AddTodo todos={this.props.todos} add={this.props.add}></AddTodo>
            </Fragment>
        );
    }
}


const mapStateToProps = state => ({todos: state.todos.todos});
const mapDispatchToProps = dispatch => ({
    add: (item) => dispatch({type: "ADD_TODO", payload: item}),
})
export default connect(mapStateToProps, mapDispatchToProps)(App)
