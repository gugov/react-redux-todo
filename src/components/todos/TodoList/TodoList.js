import React, {Component} from "react";
import {connect} from "react-redux";
import './TodoList.css'
import TodoItem from './../TodoItem/TodoItem'

class TodoList extends Component {
    render() {
        return (
            <ul className='todo__list'>
                {this.props.todos.length > 0 &&
                this.props.todos.map((item, index) => (
                    <TodoItem key={item.id} item={item} index={index} ></TodoItem>
                ))
                }
                {this.props.todos.length === 0 &&
                    <h2>No todos today!</h2>
                }
            </ul>
        )
    }
}

export default TodoList