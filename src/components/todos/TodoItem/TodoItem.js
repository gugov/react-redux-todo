import React, {Component} from "react";
import './TodoItem.css'
import {connect} from "react-redux";

class TodoItem extends Component {
    checkAsDone = (id) => {
        this.props.asDone(id)
    }

    removeTodo = (id) => {
        this.props.remove(id)
    }
    render() {
        const {
            item,
            index
        } = this.props
        return (
            <li className={item.isComplete ? 'todo__item completed' : 'todo__item'}>
                <div onClick={() => this.checkAsDone(item.id)}>
                    <span>{index + 1}. {item.text}</span> <span
                    className='todo__isCompleted'>{item.isComplete ? '(Completed)' : ''}</span>
                </div>
                <span className='todo__remove'
                      onClick={() => this.removeTodo(item.id)}>&times;</span>
            </li>
        )
    }
}

const mapStateToProps = state => ({todos: state.todos.todos});
const mapDispatchToProps = dispatch => ({
    remove: (id) => dispatch({type: "REMOVE_TODO", payload: id}),
    add: (item) => dispatch({type: "ADD_TODO", payload: item}),
    asDone: (id) => dispatch({type: "CHECK_AS_DONE", payload: id})
})
export default connect(mapStateToProps, mapDispatchToProps)(TodoItem)