import React, {Component, Fragment} from "react";
import './AddTodo.css'
import {connect} from "react-redux";

class AddTodo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            text: ''
        }
    }

    inputHandler = (e) => {
        this.setState({
            text: e.target.value
        })
    }

    addTodo = () => {
        if (this.state.text.length > 0) {
            this.props.add({
                id: new Date().getTime(),
                text: this.state.text,
                isComplete: false
            })

            this.setState({
                text: ''
            })
        }
    }
    render() {
        return (
            <Fragment>
                <input value={this.state.text} onChange={this.inputHandler} placeholder='Write your task here...'/>
                <button onClick={this.addTodo}>
                    Add Task
                </button>
            </Fragment>
        )
    }
}

export default AddTodo
